mathicgb (1.0~git20240206-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.  Closes: #1062858

 -- Graham Inggs <ginggs@debian.org>  Thu, 29 Feb 2024 12:17:42 +0000

mathicgb (1.0~git20240206-1) unstable; urgency=medium

  * New upstream release (git snapshot).
  * debian/copyright
    - Update my copyright years.

 -- Doug Torrance <dtorrance@debian.org>  Mon, 05 Feb 2024 22:42:33 -0500

mathicgb (1.0~git20231121-1) unstable; urgency=medium

  * New upstream release (git snapshot).
  * debian/control
    - Bump Standards-Version to 4.6.2.
  * debian/copyright
    - Update my copyright years.

 -- Doug Torrance <dtorrance@debian.org>  Thu, 21 Dec 2023 08:58:50 -0500

mathicgb (1.0~git20220621-2) unstable; urgency=medium

  * debian/patches/libatomic.patch
    - Restore patch; the build was still failing on mipsel and m68k
      linking with TBB without the -latomic flag.

 -- Doug Torrance <dtorrance@debian.org>  Sat, 25 Jun 2022 22:11:22 -0400

mathicgb (1.0~git20220621-1) unstable; urgency=medium

  * New upstream release (git snapshot).
  * debian/patches/big-endian.patch
    - Remove patch; issue fixed upstream.
  * debian/patches/libatomic.patch
    - Remove patch; the issue was fixed in a recent upload of the TBB
      package (2021.5.0-11).

 -- Doug Torrance <dtorrance@debian.org>  Wed, 22 Jun 2022 22:56:03 -0400

mathicgb (1.0~git20220426-2) unstable; urgency=medium

  * debian/control
    - Bump Standards-Version to 4.6.1
  * debian/patches/libatomic.patch
    - New patch; link against libatomic to avoid build failures on armel
      and mipsel.

 -- Doug Torrance <dtorrance@debian.org>  Tue, 14 Jun 2022 00:17:29 -0400

mathicgb (1.0~git20220426-1) unstable; urgency=medium

  * New upstream release (git snapshot).
  * debian/patches/{no-tbb-assert,tbb-32-bit}.patch
    - Remove patches; applied upstream

 -- Doug Torrance <dtorrance@debian.org>  Sun, 08 May 2022 20:47:38 -0400

mathicgb (1.0~git20220311-2) unstable; urgency=medium

  * debian/patches/no-tbb-assert.patch
    - New patch; include <cassert> when not building with TBB support.
  * debian/patches/tbb-32-bit.patch
    - New patch; detect TBB 2021 on 32-bit systems.

 -- Doug Torrance <dtorrance@debian.org>  Wed, 16 Mar 2022 23:12:31 -0400

mathicgb (1.0~git20220311-1) unstable; urgency=medium

  * New upstream release (git snapshot).
  * debian/gbp.conf
    - Add file; sets default Debian branch to DEP-14 recommended
     "debian/latest".
  * debian/patches/tbb-2021.patch
    - Remove patch; TBB 2021 is now supported upstream.

 -- Doug Torrance <dtorrance@debian.org>  Fri, 11 Mar 2022 14:39:03 -0500

mathicgb (1.0~git20220225-1) unstable; urgency=medium

  * New upstream release (git snapshot).
  * debian/control
    - Only include libtbb-dev in Build-Depends in Linux; it isn't available
      in Hurd or kFreeBSD.
    - Exclude libgtest-dev with nocheck build profile.
  * debian/libmathicgb-dev.install
    - Install all header files, as this release includes more than just
      mathicgb.h.
  * debian/rules
    - Build without gtest when "nocheck" in DEB_BUILD_OPTIONS.

 -- Doug Torrance <dtorrance@debian.org>  Wed, 02 Mar 2022 22:39:39 -0500

mathicgb (1.0~git20211216-3) unstable; urgency=medium

  * debian/copyright
    - Update my copyright years.
  * debian/tests/unittest
    - Drop "-std=gnu++11" to fix failing test due to lack of class
      template argument deduction before C++17.

 -- Doug Torrance <dtorrance@debian.org>  Thu, 10 Feb 2022 00:12:08 -0500

mathicgb (1.0~git20211216-2) unstable; urgency=medium

  * debian/patches/tbb-2021.patch
    - Add patch to support TBB 2021.

 -- Doug Torrance <dtorrance@debian.org>  Wed, 09 Feb 2022 16:41:27 -0500

mathicgb (1.0~git20211216-1) unstable; urgency=medium

  * New upstream release (git snapshot).
  * debian/control
    - Update my email address; now a Debian Developer.
  * debian/copyright
    - Update my email address and copyright years.
  * debian/upstream/metadata
    - Add "---" to signal document start.
    - Fix case in Bug-Database URL.
    - Add Bug-Submit URL.

 -- Doug Torrance <dtorrance@debian.org>  Wed, 22 Dec 2021 09:23:37 -0500

mathicgb (1.0~git20211001-1) unstable; urgency=medium

  * New upstream release (git snapshot).
  * debian/control
    - Update Maintainer to Debian Math Team.
    - Update Vcs-* fields (debian-science -> debian-math).
  * debian/patches/include-limits.patch
    - Remove patch; applied upstream.

 -- Doug Torrance <dtorrance@piedmont.edu>  Sun, 07 Nov 2021 17:00:14 -0500

mathicgb (1.0~git20210109-1) unstable; urgency=medium

  * New upstream release (git snapshot).
  * debian/control
    - Bump Standards-Version to 4.6.0.
    - Drop specific architectures from libtbb-dev in Build-Depends; it is
      now available on all the official ones.
  * debian/patches/big-endian.patch
    - Add Author, Bug, and Bug-Debian fields.
  * debian/patches/include-limits.patch
    - New patch; include header for std::numeric_limits. Otherwise,
      mathicgb fails to compile using GCC 11 (Closes: #984231).
  * debian/patches/link-deps.patch
    - Remove patch; applied upstream.

 -- Doug Torrance <dtorrance@piedmont.edu>  Thu, 19 Aug 2021 21:09:39 -0400

mathicgb (1.0~git20200526-1) unstable; urgency=medium

  * New upstream release (git snapshot).
  * debian/patches/cross.patch
    - Remove patch; applied upstream.
  * debian/rules
    - Remove override_dh_fixperms target; file permissions have been
      fixed upstream.
  * debian/salsa-ci.yml
    - Restore file so we don't trigger builds when pushing upstream
      or pristine-tar branches.

 -- Doug Torrance <dtorrance@piedmont.edu>  Wed, 01 Jul 2020 08:26:36 -0400

mathicgb (1.0~git20181123-2) unstable; urgency=medium

  * debian/control
    - Bump debhelper compatibility level to 13.
  * debian/not-installed
    - New file; now that we are using debhelper 13, we need to tell
      dh_missing that we are not installing the libtool .la file.
  * debian/salsa-ci.yml
    - Remove file; we will use the one hosted at the Salsa CI Team's
      repository.
  * debian/tests/control
    - Allow stderr messages in autopkgtest.  We were getting warnings from
      including tbb.h that were causing the tests to fail.  Adapted from a
      patch by Graham Inggs <ginggs@ubuntu.com> for the Ubuntu version of
      the package.

 -- Doug Torrance <dtorrance@piedmont.edu>  Thu, 14 May 2020 13:57:48 -0400

mathicgb (1.0~git20181123-1) unstable; urgency=medium

  * New upstream release (git snapshot).
  * debian/compat
    - Remove file; unnecessary after bump to compatibility level 12.
  * debian/control
    - Drop debhelper from Build-Depends in favor of debhelper-compat
      for compatibility level 12.
    - Add Rules-Requires-Root field.
    - Bump Standards-Version to 4.5.0.
  * debian/mathicgb.examples
    - Add newline at end of file.
  * debian/patches/cross.patch
    - Add patch by Helmut Grohne <helmut@subdivi.de> to allow
      cross-building (Closes: #929037).
  * debian/rules
    - Drop override_dh_strip target. It was added in package version
      1.0~git20170104-1, which is in oldstable. The target existed to
      pass the --dbgsym-migration option, and this migration is complete.
  * debian/salsa-ci.yml
    - Add Salsa pipelines config file.
  * debian/tests/control
    - Reformat using wrap-and-sort.
  * debian/upstream/metadata
    - Add Repository and Bug-Database fields.

 -- Doug Torrance <dtorrance@piedmont.edu>  Tue, 21 Apr 2020 22:25:58 -0400

mathicgb (1.0~git20170606-2) unstable; urgency=medium

  * debian/compat
    - Bump debhelper compatibility level to 11.
  * debian/control
    - Bump versioned dependency on debhelper to >= 11.
    - Bump Standards-Version to 4.1.5.
    - Update Vcs-* links to Salsa.
  * debian/rules
    - Drop get-orig-source target; replaced by uscan.
  * debian/watch
    - Use uscan with git mode to obtain upstream tarball.

 -- Doug Torrance <dtorrance@piedmont.edu>  Mon, 09 Jul 2018 22:47:45 -0400

mathicgb (1.0~git20170606-1) unstable; urgency=medium

  * New upstream release (git snapshot).
  * debian/compat
    - Bump debhelper compatibility level to 10.
  * debian/control
    - Set priority to optional.
    - Bump versioned dependency on debhelper to >= 10.
    - Remove dh-autoreconf from Build-Depends.
    - Bump Standards-Version to 4.0.1.
  * debian/copyright
    - Use https in Format.
  * debian/mathicgb.docs
    - Rename from just 'docs' for clarity.
  * debian/patches/big-endian.patch
    - New patch; fix tests on big-endian architectures (Closes: #862090).
  * debian/patches/link-deps.patch
    - New patch; properly link against mathic and memtailor.
  * debian/rules
    - Remove call to dh_autoreconf; now default with debhelper 10.

 -- Doug Torrance <dtorrance@piedmont.edu>  Sat, 12 Aug 2017 18:33:51 -0400

mathicgb (1.0~git20170104-1) unstable; urgency=medium

  * New upstream release (git snapshot).
  * debian/control
    - Bump to Standards-Version 3.9.8.
    - Remove libmathicgb-dbg package in favor of new automatically
      generated *-dbgsym packages.
  * debian/mathicgb.install
    - Move installation of manpage to dh_install.
  * debian/{mathicgb.manpages,mgb.1}
    - Remove files; manpage added upstream.
  * debian/rules
    - Add --dbgsym-migration option to dh_strip.
  * debian/tests/unittest
    - Use upstream unit tests for continuous integration.

 -- Doug Torrance <dtorrance@piedmont.edu>  Wed, 11 Jan 2017 16:46:27 -0500

mathicgb (1.0~git20150904-2) unstable; urgency=medium

  * debian/control
    - Update Vcs-Browser to use https; fixes vcs-field-uses-insecure-uri
      Lintian warning.
  * debian/{control,mgb.1}
    - Replace "Grobner" with "Groebner", the correct anglicization of "Gröbner"
      (Closes: #807904).
  * debian/rules
    - Enable all hardening flags; fixes hardening-no-{bindnow,pie} Lintian
      warnings.

 -- Doug Torrance <dtorrance@piedmont.edu>  Sun, 31 Jan 2016 16:14:51 -0500

mathicgb (1.0~git20150904-1) unstable; urgency=medium

  * New upstream release (git snapshot).
  * debian/control
    - Update Homepage.
  * debian/copyright
    - Update Source.
    - Remove Comment; license information now in source.
  * debian/patches
    - Remove files; patches applied upstream.
  * debian/rules
    - Update GTEST_PATH in override_dh_auto_configure target.
    - Add --enable-shared to override_dh_auto_configure target.
    - Update get-orig-source target with new location.
  * debian/watch
    - Update comment with new location.

 -- Doug Torrance <dtorrance@piedmont.edu>  Sat, 05 Sep 2015 20:21:41 -0400

mathicgb (1.0~git20131006-3) unstable; urgency=medium

  * Update my email address.
  * debian/control
    - Tidy up using wrap-and-sort.

 -- Doug Torrance <dtorrance@piedmont.edu>  Sat, 29 Aug 2015 09:47:58 -0400

mathicgb (1.0~git20131006-2) unstable; urgency=medium

  * debian/control
    - Move self to Uploaders; set Debian Science as Maintainer.
    - Restrict Build-Depends on libtbb-dev to the architectures on which it
      exists.
    - Set Priority to extra to satisfy Policy section 2.5 (libtbb is a
      dependency which has priority extra).
    - Remove dpkg-dev (>= 1.18.0) from Build-Depends; unnecessary as we no
      longer have a symbols file and thus do not need the arch-bits tag.
    - Remove redundant Priority field.
  * debian/libmathicgb0.symbols
    - Remove file for ease of maintainability.
  * debian/patches/fix_archs_with_unsigned_char.patch
    - New patch; fix failing tests for architectures which default to unsigned
      char.
  * debian/patches/fix_example_perms.patch
    - Remove empty file.

 -- Doug Torrance <dtorrance@monmouthcollege.edu>  Sat, 18 Jul 2015 15:06:45 -0600

mathicgb (1.0~git20131006-1) unstable; urgency=low

  * Initial release (Closes: #781665).

 -- Doug Torrance <dtorrance@monmouthcollege.edu>  Wed, 01 Apr 2015 07:12:36 -0500
